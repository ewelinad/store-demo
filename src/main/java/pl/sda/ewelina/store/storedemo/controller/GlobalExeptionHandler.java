package pl.sda.ewelina.store.storedemo.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;

@ControllerAdvice
public class GlobalExeptionHandler {

    @ExceptionHandler
    ModelAndView handleNotFoundProductExeption(NotFoundProductExeption exeption){
        ModelAndView mnv = new ModelAndView("errorPage");
        mnv.addObject("message", exeption.getMessage());
        return mnv;
    }
}
