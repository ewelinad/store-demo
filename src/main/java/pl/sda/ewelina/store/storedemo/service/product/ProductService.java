package pl.sda.ewelina.store.storedemo.service.product;

import jdk.dynalink.linker.LinkerServices;
import pl.sda.ewelina.store.storedemo.dto.CreateProductForm;
import pl.sda.ewelina.store.storedemo.dto.ProductDto;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;

import java.util.List;

public interface ProductService {

    List<ProductDto> getProducts();
    void addProduct(CreateProductForm form);
    ProductDto getProductById(long idProduct) throws NotFoundProductExeption;

}
