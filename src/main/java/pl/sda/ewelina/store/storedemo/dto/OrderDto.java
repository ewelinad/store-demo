package pl.sda.ewelina.store.storedemo.dto;

import lombok.Data;
import lombok.Setter;
import org.springframework.stereotype.Service;
import pl.sda.ewelina.store.storedemo.model.ProductEntity;

import java.time.LocalDate;
import java.util.List;

@Setter
@Data
public class OrderDto {
    private long id;
    private List<ProductDto> productEntities;
    private String status;
}
