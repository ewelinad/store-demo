package pl.sda.ewelina.store.storedemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.sda.ewelina.store.storedemo.model.UserEntity;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query("from UserEntity u left join fetch u.roles where u.login = :login")
    Optional<UserEntity> findUserWithRoleByLogin(String login);

    Optional<UserEntity> findUserEntityByLogin(String login);

    boolean existsByLogin(String login);
}
