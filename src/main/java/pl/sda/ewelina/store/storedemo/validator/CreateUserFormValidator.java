package pl.sda.ewelina.store.storedemo.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.sda.ewelina.store.storedemo.dto.RegisterUserForm;

@Component
public class CreateUserFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterUserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterUserForm form = (RegisterUserForm) o;

        if(StringUtils.isBlank(form.getLogin())){
            errors.rejectValue("login", "validator.field.notEmpty");
        }
        if(StringUtils.isBlank(form.getPassword())){
            errors.rejectValue("password", "validator.field.notEmpty");
        }
        if(StringUtils.isBlank(form.getRepeatedPassword())){
            errors.rejectValue("repeatedPassword", "validator.field.notEmpty");
        }
        if (!form.getPassword().equals(form.getRepeatedPassword())) {
            errors.rejectValue("password", "registerUserForm.validator.password.notEquals");
        }
    }
}
