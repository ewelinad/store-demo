package pl.sda.ewelina.store.storedemo.service.order;

import pl.sda.ewelina.store.storedemo.dto.OrderDto;

import java.util.List;
import java.util.Set;

public interface OrderService {

    List <OrderDto> getOrders();

    void placeOrder (List<Long> idProductList, String userLogin); //złóż zamówienie
}
