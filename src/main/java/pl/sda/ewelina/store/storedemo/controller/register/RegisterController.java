package pl.sda.ewelina.store.storedemo.controller.register;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.dto.RegisterUserForm;
import pl.sda.ewelina.store.storedemo.exeption.ExistUserException;
import pl.sda.ewelina.store.storedemo.service.user.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserService userService;
    private final Validator validator;

    @InitBinder
    void initBinding(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    public RegisterController(UserService userService, @Qualifier("createUserFormValidator") Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }



    @RequestMapping
    ModelAndView getRegisterPage() {
        ModelAndView mnv = new ModelAndView("register");
        mnv.addObject("registerUserForm", new RegisterUserForm());
        return mnv;
    }

    @PostMapping
    String addUserAction(@ModelAttribute @Validated RegisterUserForm registerUserForm, BindingResult bindingResult)
            throws ExistUserException {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.createNewUser(registerUserForm);
        return "redirect:/login";
    }

}
