package pl.sda.ewelina.store.storedemo.controller.shoppingCart;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.component.ShoppingCart;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;

@Controller
@RequestMapping
class ShoppingCartController {

    private ShoppingCart shoppingCart;

    public ShoppingCartController(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @RequestMapping("/cart/products")
    ModelAndView getProducts() {
        ModelAndView mnv = new ModelAndView("cart/products");
        mnv.addObject("productsCart", shoppingCart.getProducts());
        return mnv;
    }

    @RequestMapping("/cart/{idProduct}/addToCart")
    String addProducts(@PathVariable long idProduct) throws NotFoundProductExeption {
        shoppingCart.addProductToCart(idProduct);
        return "redirect:/cart/products";
    }

    @RequestMapping("/cart/{idProduct}/removeFromCart")
    String removeProduct(@PathVariable long idProduct) {
        shoppingCart.removeProduct(idProduct);
        return "redirect:/cart/products";
    }
    @RequestMapping("/cart/placeorder")
    String placeOrder(){
        shoppingCart.placeOrder();
        return "redirect:/orders";
    }
}
