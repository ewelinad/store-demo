package pl.sda.ewelina.store.storedemo.controller.product;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;
import pl.sda.ewelina.store.storedemo.service.product.ProductService;

@Controller
@RequestMapping("/products")
class ProductController {
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

   @RequestMapping
    ModelAndView getProductsPage() {
        ModelAndView mnv = new ModelAndView("products");
        mnv.addObject("products", productService.getProducts());
        return mnv;
    }

    @RequestMapping("/{idProduct}")
    ModelAndView getProductByIdPage(@PathVariable long idProduct) throws NotFoundProductExeption {
        ModelAndView mnv = new ModelAndView("productById");
        mnv.addObject("product", productService.getProductById(idProduct));
        return mnv;
    }
}
