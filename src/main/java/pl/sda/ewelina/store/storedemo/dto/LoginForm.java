package pl.sda.ewelina.store.storedemo.dto;

import lombok.Data;

@Data
public class LoginForm {
    private String login;
    private String password;
}
