package pl.sda.ewelina.store.storedemo.controller.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.dto.LoginForm;

@Controller
@RequestMapping("/login")
class LoginController {

    @RequestMapping
    ModelAndView getLoginPage(){
        ModelAndView mnv = new ModelAndView("login");
        mnv.addObject("loginForm", new LoginForm());
        return mnv;
    }


}
