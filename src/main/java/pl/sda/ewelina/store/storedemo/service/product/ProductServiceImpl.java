package pl.sda.ewelina.store.storedemo.service.product;

import org.springframework.stereotype.Service;
import pl.sda.ewelina.store.storedemo.dao.ProductRepository;
import pl.sda.ewelina.store.storedemo.dto.CreateProductForm;
import pl.sda.ewelina.store.storedemo.dto.ProductDto;
import pl.sda.ewelina.store.storedemo.model.ProductEntity;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;
import pl.sda.ewelina.store.storedemo.mapper.ModelMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

//    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDto> getProducts() {
        return productRepository
                .findAll()
                .stream()
                .map(ModelMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void addProduct(CreateProductForm form) {
        ProductEntity productEntity = new ProductEntity();
                productEntity.setName(form.getName());
                productEntity.setPrice(form.getPrice());
                productEntity.setDescription(form.getDescription());
        productRepository.save(productEntity);
    }

    @Override
    public ProductDto getProductById(long idProduct) throws NotFoundProductExeption {
        Optional<ProductEntity> byId = productRepository.findById(idProduct);
        return byId.map(ModelMapper::map)
                .orElseThrow(()-> new NotFoundProductExeption("Not found product with id" + idProduct));

    }
}
