package pl.sda.ewelina.store.storedemo.exeption;

public class ExistUserException extends WebAplicationExeption {

    public ExistUserException(String message) {
        super(message);
    }
}
