package pl.sda.ewelina.store.storedemo.exeption;

public class NotFoundProductExeption extends WebAplicationExeption {

    public NotFoundProductExeption(String message) {
        super(message);
    }
}
