package pl.sda.ewelina.store.storedemo.service.order;

import org.springframework.stereotype.Service;
import pl.sda.ewelina.store.storedemo.dao.OrderRepository;
import pl.sda.ewelina.store.storedemo.dao.ProductRepository;
import pl.sda.ewelina.store.storedemo.dao.UserRepository;
import pl.sda.ewelina.store.storedemo.dto.OrderDto;
import pl.sda.ewelina.store.storedemo.mapper.ModelMapper;
import pl.sda.ewelina.store.storedemo.model.OrderEntity;
import pl.sda.ewelina.store.storedemo.model.ProductEntity;
import pl.sda.ewelina.store.storedemo.model.UserEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }


    @Override
    public List<OrderDto> getOrders() {
        List<OrderEntity> orders = orderRepository.findAll();
        return orders.stream().map(ModelMapper::map).collect(Collectors.toList());

    }

    @Override
    public void placeOrder(List<Long> idProductList, String userLogin) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setStatus("NEW");
        Set<ProductEntity> collectProducts =
                idProductList
                        .stream()
                        .map(id -> productRepository.findById(id).get())
                        .collect(Collectors.toSet());
        orderEntity.setProducts(collectProducts);
        orderRepository.save(orderEntity);
        UserEntity userEntity = userRepository.findUserEntityByLogin(userLogin).get();
        userEntity.getOrders().add(orderEntity);
    }
}
