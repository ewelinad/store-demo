package pl.sda.ewelina.store.storedemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

    @RequestMapping
    public ModelAndView getIndexPage (){
        ModelAndView mnv = new ModelAndView("index");
        mnv.addObject("userName", "Ewelina");
        return mnv;
    }
}
