package pl.sda.ewelina.store.storedemo.mapper;

import pl.sda.ewelina.store.storedemo.dto.OrderDto;
import pl.sda.ewelina.store.storedemo.dto.ProductDto;
import pl.sda.ewelina.store.storedemo.model.OrderEntity;
import pl.sda.ewelina.store.storedemo.model.ProductEntity;

import java.util.stream.Collectors;


public final class ModelMapper {
    private ModelMapper() {
    }

    public static ProductDto map(ProductEntity productEntity) {

        ProductDto productDto =
                new ProductDto(productEntity.getId(),
                        productEntity.getName(),
                        productEntity.getPrice(),
                        productEntity.getDescription());
        return productDto;
    }

    public static OrderDto map(OrderEntity orderEntity){
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderEntity.getId());
        orderDto.setStatus(orderEntity.getStatus());
        orderDto.setProductEntities(orderEntity
                .getProducts()
                .stream()
                .map(ModelMapper::map).collect(Collectors.toList()));
        return orderDto;
    }
}
