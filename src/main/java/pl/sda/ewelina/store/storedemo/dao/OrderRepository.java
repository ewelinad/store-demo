package pl.sda.ewelina.store.storedemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import pl.sda.ewelina.store.storedemo.model.OrderEntity;

import java.util.List;

public interface OrderRepository extends JpaRepository <OrderEntity, Long> {
    List<OrderEntity> findAll();

}
