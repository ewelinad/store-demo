package pl.sda.ewelina.store.storedemo.controller.admin;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.ewelina.store.storedemo.dto.CreateProductForm;
import pl.sda.ewelina.store.storedemo.service.product.ProductService;


@Controller
@RequestMapping("/admin")
class AdminController {

    private final ProductService productService;
    private final Validator validator;

    @InitBinder
    public void initBinding(WebDataBinder binder){
        binder.setValidator(validator);
    }

    //@Autowired - nie potrzebne od którejśtam wersji springa
    AdminController(ProductService productService, @Qualifier("createProductFormValidator") Validator validator) {
        this.productService = productService;
        this.validator = validator;
    }

    @RequestMapping("/products")
    ModelAndView getProductsPage() {
        ModelAndView mnv = new ModelAndView("admin/products");
        mnv.addObject("products", productService.getProducts());
        return mnv;
    }

    @RequestMapping("/products/newproduct")
    ModelAndView addProductForm() {
        ModelAndView mnv = new ModelAndView("admin/newProduct");
        mnv.addObject("createProductForm", new CreateProductForm());

        return mnv;
    }

    @PostMapping("/products/newproduct")
    String addNewProduct(@ModelAttribute @Validated CreateProductForm createProductForm,
                         BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return "redirect:/admin/newProduct";
        }
        productService.addProduct(createProductForm);
        return "redirect:/admin/products";
    }

    //TODO:
    @RequestMapping("/orders")
    ModelAndView getOrderPage() {
        return new ModelAndView("admin/orders");
    }
}
