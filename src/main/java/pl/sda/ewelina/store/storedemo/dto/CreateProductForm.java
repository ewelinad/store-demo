package pl.sda.ewelina.store.storedemo.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CreateProductForm {
    private String name;
    private BigDecimal price;
    private String description;

    @Override
    public String toString() {
        return "CreateProductForm{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
