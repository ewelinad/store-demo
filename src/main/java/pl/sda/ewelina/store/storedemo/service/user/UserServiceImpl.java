package pl.sda.ewelina.store.storedemo.service.user;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.ewelina.store.storedemo.dao.UserRepository;
import pl.sda.ewelina.store.storedemo.dao.UserRoleEntityRepository;
import pl.sda.ewelina.store.storedemo.dto.RegisterUserForm;
import pl.sda.ewelina.store.storedemo.exeption.ExistUserException;
import pl.sda.ewelina.store.storedemo.model.UserEntity;
import pl.sda.ewelina.store.storedemo.model.UserRoleEntity;


@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleEntityRepository userRoleEntityRepository;


    @Override
    @Transactional
    public void createNewUser(RegisterUserForm userForm) throws ExistUserException {
        if(userRepository.existsByLogin(userForm.getLogin())){
            throw new ExistUserException("Exist user with login:" + userForm.getLogin());
        }

        String encodedPassword = passwordEncoder.encode(userForm.getPassword());

        UserEntity userEntity = new UserEntity(userForm.getLogin(), encodedPassword);
        if(userEntity.getLogin().equalsIgnoreCase("admin")){
            UserRoleEntity admin = userRoleEntityRepository.findByName("ADMIN");
            userEntity.getRoles().add(admin);
        }else{
            UserRoleEntity user = userRoleEntityRepository.findByName("USER");
            userEntity.getRoles().add(user);
        }

        userRepository.save(userEntity);
    }
}
