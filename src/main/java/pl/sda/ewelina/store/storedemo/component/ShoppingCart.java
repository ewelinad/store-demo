package pl.sda.ewelina.store.storedemo.component;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.sda.ewelina.store.storedemo.dto.ProductDto;
import pl.sda.ewelina.store.storedemo.exeption.NotFoundProductExeption;
import pl.sda.ewelina.store.storedemo.service.order.OrderService;
import pl.sda.ewelina.store.storedemo.service.product.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ShoppingCart {

    private List<ProductDto> productDtos = new ArrayList<>();

    private final ProductService productService;
    private final OrderService orderService;

    public ShoppingCart(ProductService productService, OrderService orderService) {
        this.productService = productService;
        this.orderService = orderService;
    }

    public void addProductToCart(long idProduct) throws NotFoundProductExeption {
        productDtos.add(productService.getProductById(idProduct));
    }

    public void removeProduct(long idProduct){
        productDtos.removeIf(productDto -> productDto.getId() == idProduct);
    }

    public List<ProductDto> getProducts(){
        return productDtos;
    }

    public void placeOrder(){
        String userLogin = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();

        orderService.placeOrder(productDtos.stream().map(ProductDto::getId).collect(Collectors.toList()), userLogin);
        productDtos.clear();
    }
}
