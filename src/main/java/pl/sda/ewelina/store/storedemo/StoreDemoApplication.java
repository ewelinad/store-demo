package pl.sda.ewelina.store.storedemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.sda.ewelina.store.storedemo.dao.UserRoleEntityRepository;
import pl.sda.ewelina.store.storedemo.model.UserRoleEntity;

@SpringBootApplication
public class StoreDemoApplication implements CommandLineRunner {

    @Autowired
    private UserRoleEntityRepository userRoleEntityRepository;

    public static void main(String[] args) {
        SpringApplication.run(StoreDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if(!userRoleEntityRepository.existsByName("ADMIN")){
            userRoleEntityRepository.save(new UserRoleEntity("ADMIN"));
        }

        if(!userRoleEntityRepository.existsByName("USER")){
            userRoleEntityRepository.save(new UserRoleEntity("USER"));
        }
    }
}
