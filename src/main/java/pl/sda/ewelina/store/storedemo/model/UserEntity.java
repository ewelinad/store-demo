package pl.sda.ewelina.store.storedemo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@NoArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String login;

    private String password;

    @OneToMany
    @JoinColumn(name="id_user")
    private List<OrderEntity> orders = new ArrayList<>();

    @ManyToMany
    private Set<UserRoleEntity> roles = new HashSet<>();

    public UserEntity (String login, String password){
        this.login = login;
        this.password = password;
    }
}
