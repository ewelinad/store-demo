package pl.sda.ewelina.store.storedemo.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.sda.ewelina.store.storedemo.dto.CreateProductForm;

@Component
public class CreateProductFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return CreateProductForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CreateProductForm form = (CreateProductForm) o;
        if (StringUtils.isBlank(form.getName())){
            errors.rejectValue("name", "validator.field.notEmpty");
        }
    }
}
