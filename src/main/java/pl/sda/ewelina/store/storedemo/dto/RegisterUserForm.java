package pl.sda.ewelina.store.storedemo.dto;

import lombok.Data;

@Data
public class RegisterUserForm {
    private String login;
    private String password;
    private String repeatedPassword;
}
