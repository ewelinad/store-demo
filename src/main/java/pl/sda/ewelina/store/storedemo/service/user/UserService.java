package pl.sda.ewelina.store.storedemo.service.user;


import pl.sda.ewelina.store.storedemo.dto.RegisterUserForm;
import pl.sda.ewelina.store.storedemo.exeption.ExistUserException;
import pl.sda.ewelina.store.storedemo.model.UserEntity;

import java.util.List;

public interface UserService {
    void createNewUser(RegisterUserForm userForm) throws ExistUserException;
}
