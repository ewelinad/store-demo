package pl.sda.ewelina.store.storedemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.ewelina.store.storedemo.model.UserRoleEntity;

public interface UserRoleEntityRepository extends JpaRepository<UserRoleEntity, Long> {
    boolean existsByName(String name);
    UserRoleEntity findByName(String name);
}
